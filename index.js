let accidentsAll = document.getElementById("accidents__all");
let accidentsSelected = document.getElementById("accidents__selected");

function getColor(number) {
    let maxN = 427;
    let minN = 0;
    let color = "";

    let minRed = 63, halfRed = 190, maxRed = 236;
    let stepRedA = (halfRed - minRed) / 50;
    let stepRedB = (maxRed - halfRed) / 50;

    let minGreen = 226, halfGreen = 184, maxGreen = 40;
    let stepGreenA = (halfGreen - minGreen) / 50;
    let stepGreenB = (maxGreen - halfGreen) / 50;

    let minBlue = 31, halfBlue = 15, maxBlue = 58;
    let stepBlueA = (halfBlue - minBlue) / 50;
    let stepBlueB = (maxBlue - halfBlue) / 50;

    let red = 0;
    let green = 0;
    let blue = 0;

    accidentsAll.style.color = "rgb(" + maxRed + "," + maxGreen + "," + maxBlue + ")";
    accidentsAll.innerHTML = maxN;

    function getNumber(n) {
        if (n <= minN) {
            color = "rgb(" + minRed + "," + minGreen + "," + minBlue + ")";
        } else if (n >= maxN) {
            color = "rgb(" + maxRed + "," + maxGreen + "," + maxBlue + ")";
        } else if (n === (maxN / 2)) {
            color = "rgb(" + halfRed + "," + halfGreen + "," + halfBlue + ")";
        } else  if (n < (maxN / 2)) {
            let percent = n / maxN * 100;
            red = Math.floor(stepRedA * percent + minRed);
            green = Math.floor(stepGreenA * percent + minGreen);
            blue = Math.floor(stepBlueA * percent + minBlue);
            color = "rgb(" + red + "," + green + "," + blue + ")";
        } else  if (n > (maxN / 2)) {
            let percent = (n / maxN * 100) - 50;
            red = Math.floor(stepRedB * percent + halfRed);
            green = Math.floor(stepGreenB * percent + halfGreen);
            blue = Math.floor(stepBlueB * percent + halfBlue);
            color = "rgb(" + red + "," + green + "," + blue + ")";
        }
    }

    getNumber(number);
    accidentsSelected.innerHTML = number;
    accidentsSelected.style.color = color;
}

getColor(150);